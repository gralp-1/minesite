## Developing

Once you've created a project and installed dependencies with `yarn install` start a development server:

```bash
yarn run dev

# or start the server and open the app in a new browser tab
yarn run dev -- --open
```